#include "client.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

typedef struct Client {
    int socket;
    int domain;
    int type;
    int protocol;
    struct sockaddr_in address;
    int port;
} Client;

Client *client_create(int domain, int type, int protocol, char *address, int port)
{
    Client *client = malloc(sizeof (Client));
    if(!client)
        return NULL;

    client->socket = socket(domain, type, protocol);
    if(client->socket < 0)
        return NULL;

    client->domain = domain;
    client->type = type;
    client->protocol = protocol;

    client->address.sin_family = domain;
    client->address.sin_addr.s_addr = inet_addr(address);
    client->address.sin_port = htons(port);

    return client;
}

bool client_connect(Client *client)
{
    if(connect(client->socket, (struct sockaddr*)&client->address, sizeof client->address) < 0)
        return false;

    return true;
}

bool client_send(Client *client, char *message)
{
    if(send(client->socket, message, strlen(message), 0) < 0)
        return false;

    return true;
}

bool client_recv(Client *client)
{
    char *message;
    if(recv(client->socket, message, strlen(message), 0) < 0)
        return false;

    return true;
}

bool client_sendto(Client *client, char *message)
{
    socklen_t server_address_size = sizeof server->address;
    if(sendto(server->socket, message, strlen(message), 0, (struct sockaddr*)&client->address, server_address_size) < 0)
        return false;

    return true;
}

bool client_recvfrom(Client *client)
{
    char *message;
    socklen_t client_address_size = sizeof client->address;
    if(recvfrom(client->socket, message, sizeof(message), 0, (struct sockaddr*)&client->address, &client_address_size) < 0)
        return false;

    return true;
}
