#ifndef CLIENT_H
#define CLIENT_H

#include <arpa/inet.h>

#include <stdbool.h>
#include <string.h>
#include <time.h>

typedef struct Client Client;

Client *client_create(int domain, int type, int protocol, char *address, int port);
bool client_connect(Client *client);

bool client_send(Client *client, char *message);
bool client_recv(Client *client);
bool client_sendto(Client *client, char *message);
bool client_recvfrom(Client *client);

#endif
