#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <arpa/inet.h>

char *IP = "127.0.0.1";
int PORT = 6969;

int main()
{
    int client_socket;
    char server_message[1024], client_message[1024];
    ssize_t client_sendto, client_recvfrom;
    struct sockaddr_in server_address, client_address;
    socklen_t server_address_size, client_address_size;

    server_address.sin_family = AF_INET;
    server_address.sin_addr.s_addr = inet_addr(IP);
    server_address.sin_port = htons(PORT);

    client_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if(client_socket < 0) {
        perror("[-] socket");
        exit(1);
    }
    printf("[+] socket\n");
    
    memset(&client_message, 0, 1024);
    strcpy(client_message, "hello");
    server_address_size = sizeof server_address;
    client_sendto = sendto(client_socket, client_message, strlen(client_message), 0,
        (struct sockaddr*)&server_address, server_address_size);
    if(client_sendto < 0) {
        perror("[-] sendfrom");
        exit(1);
    }
    printf("[+] sendto %s\n", client_message);

    memset(&server_message, 0, 1024);
    client_address_size = sizeof client_address;
    client_recvfrom = recvfrom(client_socket, server_message, sizeof(server_message), 0,
        (struct sockaddr*)&client_address, &client_address_size);
    if(client_recvfrom < 0) {
        perror("[-] recvfrom: %s\n");
        exit(1);
    }
    printf("[+] recvfrom %s\n", server_message);
}
