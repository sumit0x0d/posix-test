#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <arpa/inet.h>

char *IP = "127.0.0.1";
int PORT = 6969;

int main()
{
    int server_socket, server_bind, server_listen, client_accept;
    char server_message[1024], client_message[1024];
    ssize_t server_sendto, server_recvfrom;
    struct sockaddr_in server_address, client_address;
    socklen_t client_address_size;

    server_address.sin_family = AF_INET;
    server_address.sin_addr.s_addr = inet_addr(IP);
    server_address.sin_port = htons(PORT);

    server_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if(server_socket < 0) {
        perror("[-] socket");
        exit(1);
    }
    printf("[+] socket\n");

    server_bind = bind(server_socket, (struct sockaddr*)&server_address, sizeof server_address);
    if(server_bind < 0) {
        perror("[-] bind");
        exit(1);
    }
    printf("[+] bind %s:%d\n", IP, PORT);

    client_address_size = sizeof client_address;
    memset(&client_message, 0, 1024);
    server_recvfrom = recvfrom(server_socket, client_message, sizeof(client_message), 0,
        (struct sockaddr*)&client_address, &client_address_size);
    if(server_recvfrom < 0) {
        perror("[-] recvfrom: %s\n");
        exit(1);
    }
    printf("[+] recvfrom %s\n", client_message);

    memset(&server_message, 0, 1024);
    strcpy(server_message, "hi");
    server_sendto = sendto(server_socket, server_message, strlen(server_message), 0,
        (struct sockaddr*)&client_address, client_address_size);
    if(server_sendto < 0) {
        perror("[-] sendto: %s\n");
        exit(1);
    }
    printf("[+] sendto %s\n", server_message);
}
