#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <arpa/inet.h>

char *IP = "127.0.0.1";
int PORT = 6969;

int main()
{
    int server_socket, server_bind, server_listen, client_accept;
    char server_message[1024], client_message[1024];
    ssize_t server_send, server_recv;
    struct sockaddr_in server_address, client_address;
    socklen_t client_address_size;

    server_address.sin_family = AF_INET;
    server_address.sin_addr.s_addr = inet_addr(IP);
    server_address.sin_port = htons(PORT);

    server_socket = socket(AF_INET, SOCK_STREAM, 0);
    if(server_socket < 0) {
        perror("[-] socket");
        exit(1);
    }
    printf("[+] socket\n");

    server_bind = bind(server_socket, (struct sockaddr*)&server_address, sizeof server_address);
    if(server_bind < 0) {
        perror("[-] bind");
        exit(1);
    }
    printf("[+] bind %s:%d\n", IP, PORT);

    server_listen = listen(server_socket, 5);
    if(server_listen < 0) {
        perror("[-] listen");
        exit(1);
    }
    printf("[+] listen\n");

    while(true) {
        client_address_size = sizeof client_address;
        client_accept = accept(server_socket, (struct sockaddr*)&client_address, &client_address_size);
        if(client_accept < 0) {
            perror("[-] accept");
            exit(1);
        }
        printf("[+] accept\n");

        memset(&client_message, 0, 1024);
        server_recv = recv(client_accept, client_message, sizeof client_message, 0);
        if(server_recv < 0) {
            perror("[-] recv");
            exit(1);
        }
        printf("[+] recv: %s\n", client_message);

        memset(&server_message, 0, 1024);
        strcpy(server_message, "Hi");
        server_send = send(client_accept, server_message, strlen(server_message), 0);
        if(server_send < 0) {
            perror("[-] send");
            exit(1);
        }
        printf("[+] send: %s\n", server_message);

        // close(server_socket);
    }
}
