#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/socket.h>
#include <arpa/inet.h>

char *IP = "127.0.0.1";
int PORT = 6969;

int main()
{
    int client_socket, client_connect;
    char server_message[1024], client_message[1024];
    ssize_t client_send, client_recv;
    struct sockaddr_in client_address;

    client_socket = socket(AF_INET, SOCK_STREAM, 0);
    if(client_socket < 0) {
        perror("[-] socket");
        exit(1);
    }
    printf("[+] socket\n");

    client_address.sin_family = AF_INET;
    client_address.sin_addr.s_addr = inet_addr(IP);
    client_address.sin_port = htons(PORT);

    client_connect = connect(client_socket, (struct sockaddr*)&client_address, sizeof client_address);
    if(client_connect < 0) {
        perror("[-] connect");
        exit(1);
    }
    printf("[+] connect %s:%d\n", IP, PORT);

    memset(&client_message, 0, 1024);
    strcpy(client_message, "Hello");
    client_send = send(client_socket, client_message, strlen(client_message), 0);
    if(client_send < 0) {
        perror("[-] send");
        exit(1);
    }
    printf("[+] send: %s\n", client_message);

    memset(&server_message, 0, 1024);
    client_recv = recv(client_socket, server_message, sizeof server_message, 0);
    if(client_recv < 0) {
        perror("[-] recv");
        exit(1);
    }
    printf("[+] recv: %s\n", server_message);

    // close(client_socket);
}
