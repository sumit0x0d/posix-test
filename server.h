#ifndef SERVER_H
#define SERVER_H

#include <arpa/inet.h>

#include <stdbool.h>
#include <string.h>
#include <time.h>

typedef struct Server Server;
typedef struct Client Client;

Server *server_create(int domain, int type, int protocol, char *address, int port, int connection);
bool server_bind(Server *server);
bool server_listen(Server *server);
Client *client_accept(Server *server, size_t message_size);

bool server_send(Client *client, char *message);
bool server_recv(Client *client);
bool server_sendto(Client *client, char *message);
bool server_recvfrom(Client *client);

#endif
