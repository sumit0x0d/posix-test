#include "server.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

typedef struct Server {
    int socket;
    int domain;
    int type;
    int protocol;
    struct sockaddr_in address;
    int port;
    int connection;
} Server;

typedef struct Message {
    char *data;
    size_t data_size;
    char *to;
    char *from;
    time_t time;
} Message;

typedef struct Client {
    int socket;
    struct sockaddr_in address;
    Message message;
} Client;

Server *server_create(int domain, int type, int protocol, char *address, int port, int connection)
{
    Server *server = malloc(sizeof (Server));
    if(!server)
        return NULL;

    server->socket = socket(domain, type, protocol);
    if(server->socket < 0)
        return NULL;

    server->domain = domain;
    server->type = type;
    server->protocol = protocol;

    server->address.sin_family = domain;
    server->address.sin_addr.s_addr = inet_addr(address);
    server->address.sin_port = htons(port);

    server->connection = connection;

    return server;
}

bool server_bind(Server *server)
{
    if(bind(server->socket, (struct sockaddr*)&server->address, sizeof server->address) < 0)
        return false;

    return true;
}

bool server_listen(Server *server)
{
    if(listen(server->socket, server->connection) < 0)
        return false;

    return true;
}

Client *client_accept(Server *server, size_t message_size)
{
    Client *client = malloc(sizeof (Client));
    if(!client)
        return NULL;

    socklen_t client_address_size = sizeof client->address;

    client->socket = accept(server->socket, (struct sockaddr*)&client->address, &client_address_size);
    if(client->socket < 0) return NULL;

    client->message.data_size = message_size;

    client->message.data = calloc(message_size, sizeof (char));
    if(!client->message.data)
        return NULL;

    close(server->socket);

    return client;
}

bool server_send(Client *client, char *message)
{
    if(send(client->socket, message, strlen(message), 0) < 0)
        return false;

    client->message.data = malloc(client->message.data_size);
    if(!client->message.data)
        return false;

    strncpy(client->message.data, message, client->message.data_size);

    // client->message.to = "";
    // client->message.from = client->address.sin_addr.s_addr;
    client->message.time = time(0);

    return true;
}

bool server_recv(Client *client)
{
    char message[client->message.data_size];
    if(recv(client->socket, message, client->message.data_size, 0) < 0)
        return false;

    client->message.data = malloc(client->message.data_size);
    if(!client->message.data)
        return false;

    strncpy(client->message.data, message, client->message.data_size);

    // client->message.to = "";
    // client->message.from = client->address.sin_addr.s_addr;
    client->message.time = time(0);

    return true;
}

bool server_sendto(Client *client, char *message)
{

}

bool server_recvfrom(Client *client)
{

}
